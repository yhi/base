package com.pisome.tool;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.SharedPreferencesCompat;

import java.util.Collections;
import java.util.Set;

public class Configs
{
  static private SharedPreferences S;
  static final public boolean DEFAULT_BOOLEAN = Boolean.FALSE;
  static final public float DEFAULT_FLOAT = Float.NaN;
  static final public int DEFAULT_INT = Integer.MIN_VALUE;
  static final public long DEFAULT_LONG = Integer.MIN_VALUE;
  static final public String DEFAULT_STRING = "";
  static final public Set<String> DEFAULT_SET = Collections.emptySet();

  static public void init(Context ctx)
  {
    S = ctx.getSharedPreferences(ctx.getPackageName(), Context.MODE_PRIVATE);
//      .getDefaultSharedPreferences(ctx.getApplicationContext());
  }

  private Configs() {}

  static private void apply(SharedPreferences.Editor editor)
  {
    SharedPreferencesCompat.EditorCompat.getInstance().apply(editor);
  }

  static public void setFloat(String key, float value)
  {
    SharedPreferences.Editor e = S.edit();
    e.putFloat(key, value);
    apply(e);
  }

  static public void setString(String key, String value)
  {
    SharedPreferences.Editor e = S.edit();
    e.putString(key, value);
    apply(e);
  }

  static public void setInt(String key, int value)
  {
    SharedPreferences.Editor e = S.edit();
    e.putInt(key, value);
    apply(e);
  }

  static public void setBoolean(String key, boolean value)
  {
    SharedPreferences.Editor e = S.edit();
    e.putBoolean(key, value);
    apply(e);
  }

  static public void setLong(String key, long value)
  {
    SharedPreferences.Editor e = S.edit();
    e.putLong(key, value);
    apply(e);
  }

  static public void setStrings(String key, Set<String> s)
  {
    SharedPreferences.Editor e = S.edit();
    e.putStringSet(key, s);
    apply(e);
  }

  static public String getString(String key)
  {
    return S.getString(key, DEFAULT_STRING);
  }

  static public String getString(String key, String value)
  {
    return S.getString(key, value);
  }

  static public boolean getBoolean(String key)
  {
    return S.getBoolean(key, DEFAULT_BOOLEAN);
  }

  static public boolean getBoolean(String key, boolean value)
  {
    return S.getBoolean(key, value);
  }

  static public int getInt(String key)
  {
    return S.getInt(key, DEFAULT_INT);
  }
  static public int getInt(String key, int value)
  {
    return S.getInt(key, value);
  }

  static public long getLong(String key)
  {
    return S.getLong(key, DEFAULT_LONG);
  }
  static public long getLong(String key, long value)
  {
    return S.getLong(key, value);
  }

  static public float getFloat(String key)
  {
    return S.getFloat(key, DEFAULT_FLOAT);
  }

  static public float getFlaot(String key, float value)
  {
    return S.getFloat(key, value);
  }

  static public Set<String> getStrings(String key)
  {
    return S.getStringSet(key, DEFAULT_SET);
  }

  static public Set<String> getStrings(String key, Set<String> values)
  {
    return S.getStringSet(key, values);
  }
}