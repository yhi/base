package com.pisome.tool;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.util.Pair;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Surface;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.lang.ref.WeakReference;

//import static io.keymon.KMA.CTX;

public final class Screens
{
  private static final String TAG = Screens.class.getSimpleName();

  private static final Point P = new Point();

  private static WeakReference<Context> CTX_WRAPPER;

  private static float DENSITY;
  private static Type DPI;

  public static void init(Context ctx)
  {
    CTX_WRAPPER = new WeakReference<>(ctx.getApplicationContext());
    DisplayMetrics dspm = ctx.getResources().getDisplayMetrics();
    DENSITY = dspm.density;
    DPI = Type.from(dspm.densityDpi);
  }

  private Screens() {}

  public enum Type
  {
    LDPI(DisplayMetrics.DENSITY_LOW),
    MDPI(DisplayMetrics.DENSITY_MEDIUM),
    HDPI(DisplayMetrics.DENSITY_HIGH),
    XHDPI(DisplayMetrics.DENSITY_XHIGH),
    @SuppressLint("InlinedApi") XXHDPI((Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) ? DisplayMetrics.DENSITY_XXHIGH : 480),
    @SuppressLint("InlinedApi") XXXHDPI((Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) ? DisplayMetrics.DENSITY_XXXHIGH : 640);

    int dpi;

    @Override
    public String toString()
    {
      return super.toString().toLowerCase();
    }

    public static Type from(int i)
    {
      switch (i)
      {
        case DisplayMetrics.DENSITY_LOW:
          return LDPI;
        case DisplayMetrics.DENSITY_MEDIUM:
          return MDPI;
        case DisplayMetrics.DENSITY_TV:
        case DisplayMetrics.DENSITY_HIGH:
          return HDPI;
        case DisplayMetrics.DENSITY_XHIGH:
          return XHDPI;
        case DisplayMetrics.DENSITY_400:
        case DisplayMetrics.DENSITY_XXHIGH:
          return XXHDPI;
        case DisplayMetrics.DENSITY_560:
        case DisplayMetrics.DENSITY_XXXHIGH:
          return XXXHDPI;
        default:
          return MDPI;
      }
    }

    Type(int dpi) { this.dpi = dpi; }
  }

  public enum Orientation
  {
    PORTRAIT(Configuration.ORIENTATION_PORTRAIT, Surface.ROTATION_0),
    LANDSCAPE(Configuration.ORIENTATION_LANDSCAPE, Surface.ROTATION_90),
    PORTRAIT_180(Configuration.ORIENTATION_PORTRAIT, Surface.ROTATION_180),
    LANDSCAPE_270(Configuration.ORIENTATION_LANDSCAPE, Surface.ROTATION_270);

    int pl;
    int r;

    Orientation(int pl, int r)
    {
      this.pl = pl;
      this.r = r;
    }

    public boolean isPortrait()
    {
      return this.pl == Configuration.ORIENTATION_PORTRAIT;
    }

    public int rotation()
    {
      return this.r;
    }
  }

  public static boolean isScreenOn()
  {
    Context ctx = CTX_WRAPPER.get();
    PowerManager pm = (PowerManager)ctx.getSystemService(Context.POWER_SERVICE);
    return pm.isScreenOn();
  }

  public static Orientation orientation()
  {
    Context ctx = CTX_WRAPPER.get();
    int o = ((WindowManager)(ctx.getSystemService(Context.WINDOW_SERVICE))).getDefaultDisplay().getRotation();

    switch (o)
    {
      case Surface.ROTATION_0:
        return Orientation.PORTRAIT;
      case Surface.ROTATION_90:
        return Orientation.LANDSCAPE;
      case Surface.ROTATION_180:
        return Orientation.PORTRAIT_180;
      case Surface.ROTATION_270:
        return Orientation.LANDSCAPE_270;
      default:
        return null;
    }
  }

  public static Pair<Integer, Integer> calculateSquareCellSizePx(ViewGroup.MarginLayoutParams glps, int minCellWidthPx, int hSpacePx)
  {
    Pair<Integer, Integer> m = Screens.maxPX();
    int mw = m.first - glps.leftMargin - glps.rightMargin;
    int c = 1;
    while (c * minCellWidthPx + (c - 1) * hSpacePx <= mw) { c++; }
    int l = (int)((mw - (c - 1) * hSpacePx) * 1.0F / c);
    Pair<Integer, Integer> s = new Pair<>(l, l);
//    Log.d(TAG, "[calculateCellSize] " + mw + "/" + m + "/" + c + "/" + s);
    return s;
  }

  public static int dp2px(int dp)
  {
    return Math.round(dp * DENSITY);
  }

  public static int px2dp(int px)
  {
    return Math.round(px / DENSITY);
  }

  public static Pair<Integer, Integer> maxPX()
  {
    Context ctx = CTX_WRAPPER.get();
    Display dsp = ((WindowManager)(ctx.getSystemService(Context.WINDOW_SERVICE))).getDefaultDisplay();
    dsp.getSize(P);
    return new Pair<>(P.x, P.y);
  }

  public static Pair<Integer, Integer> maxDP()
  {
    Pair<Integer, Integer> s = Screens.maxPX();
    return new Pair<>(Screens.px2dp(s.first), Screens.px2dp(s.second));
  }

  public static Pair<Integer, Integer> displayMaxPX()
  {
    Context ctx = CTX_WRAPPER.get();
    Display dsp = ((WindowManager)(ctx.getSystemService(Context.WINDOW_SERVICE))).getDefaultDisplay();
    if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17)
    {
      try
      {
        int w = (Integer)Display.class.getMethod("getRawWidth").invoke(dsp);
        int h = (Integer)Display.class.getMethod("getRawHeight").invoke(dsp);
        return new Pair<>(w, h);
      }
      catch (Exception e)
      {
        Log.e(TAG, "[displayMaxPX] " + e.getMessage(), e);
      }
    }
    else if (Build.VERSION.SDK_INT >= 17)
    {
      dsp.getRealSize(P);
      return new Pair<>(P.x, P.y);
    }
    return Screens.maxPX();
  }

  public static Pair<Integer, Integer> displayMaxDP()
  {
    Pair<Integer, Integer> s = Screens.displayMaxPX();
    return new Pair<>(Screens.px2dp(s.first), Screens.px2dp(s.second));
  }

  public static int actionBarHeight(Context ctx)
  {
    TypedValue typedValue = new TypedValue();
    int[] textSizeAttr = new int[] { android.R.attr.actionBarSize };
    int indexOfAttrTextSize = 0;
    TypedArray a = ctx.obtainStyledAttributes(typedValue.data, textSizeAttr);
    int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
    a.recycle();
    return actionBarSize;
  }

  public static int statusBarHeight(Context ctx)
  {
    int result = 0;
    int resourceId = ctx.getResources().getIdentifier("status_bar_height", "dimen", "android");
    if (resourceId > 0)
    {
      result = ctx.getResources().getDimensionPixelSize(resourceId);
    }
    return result;
  }
}
