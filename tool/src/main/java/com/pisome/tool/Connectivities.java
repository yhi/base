package com.pisome.tool;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.lang.ref.WeakReference;

public class Connectivities
{
  private static final String TAG = "Connectivities";
  private static final Integer[] NON_CELLULARS = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_ETHERNET, ConnectivityManager.TYPE_WIMAX};
  private static final Integer[] CELLULARS = {ConnectivityManager.TYPE_MOBILE};
  private static final Integer[] ALL = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_ETHERNET, ConnectivityManager.TYPE_WIMAX, ConnectivityManager.TYPE_MOBILE};
  private static WeakReference<Context> CTX_WRAPPER;

  private Connectivities() {}

  public synchronized static void init(Context ctx, ChangeListener listener)
  {
    if (CTX_WRAPPER == null)
    {
      CTX_WRAPPER = new WeakReference<>(ctx.getApplicationContext());
      Connectivities.listener = listener;
    }
  }

  public interface ChangeListener
  {
    void onConnected();
    void onDisconnected();
  }

  private static ChangeListener listener;

  public static State getState()
  {
    return isConnected() ? State.CONNECTED : State.DISCONNECTED;
  }

  public static boolean isConnected()
  {
    return isConnected(ALL);
  }

  private static boolean isConnected(Integer[] types)
  {
    Context ctx = CTX_WRAPPER.get();
    ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();

    if (activeNetworkInfo != null)
    {
      if (!activeNetworkInfo.isConnectedOrConnecting())
      {
        return false;
      }

      for (int type : types)
      {
        if (activeNetworkInfo.getType() == type)
        {
          return true;
        }
      }
    }

    return false;
  }

  public static Type getType()
  {
    if (isCellularConnectivity())
    {
      return Type.CELLULAR_CONNECTED;
    }
    else if (isNonCellularConnectivity())
    {
      return Type.NON_CELLULAR_CONNECTED;
    }

    return Type.NOT_CONNECTED;
  }

  public static boolean isCellularConnectivity()
  {
    return isConnected(CELLULARS);
  }

  public static boolean isNonCellularConnectivity()
  {
    return isConnected(NON_CELLULARS);
  }

  public enum State
  {
    CONNECTED, DISCONNECTED
  }

  public enum Type
  {
    NOT_CONNECTED, CELLULAR_CONNECTED, NON_CELLULAR_CONNECTED
  }

  public static void toggle(boolean on)
  {
    Context ctx = CTX_WRAPPER.get();
    ComponentName receiver = new ComponentName(ctx, ChangeReceiver.class);
    PackageManager pm = ctx.getPackageManager();
    pm.setComponentEnabledSetting(receiver,
    on ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
    PackageManager.DONT_KILL_APP);
  }

  public static class ChangeReceiver extends BroadcastReceiver
  {
    private static final String TAG = "ChangeReceiver";

    @Override
    public void onReceive(Context context, Intent intent)
    {
      Connectivities.State state = Connectivities.getState();

      Log.d(TAG, "[onReceive] connectivity = " + Connectivities.getType() + ", getState = " + state);

      switch (state)
      {
        case CONNECTED:
          Connectivities.listener.onConnected();
          break;
        case DISCONNECTED:
          Connectivities.listener.onDisconnected();
          break;
      }
    }
  }
}