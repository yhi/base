package com.pisome.tool;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class KoreansUnitTest
{
  @Test
  public void testSuffix() throws Exception
  {
    assertEquals(4, 2 + 2);
    assertEquals(Koreans.attachYneOrNe("마마무"), "마마무네");
    assertEquals(Koreans.attachYneOrNe("강문"), "강문이네");
  }
}