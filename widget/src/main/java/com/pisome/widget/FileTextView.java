package com.pisome.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class FileTextView extends TextView
{
  private static final String TAG = FileTextView.class.getSimpleName();
  private String fileName;

  public FileTextView(Context context)
  {
    super(context);
    init(null, 0);
  }

  public FileTextView(Context context, AttributeSet attrs)
  {
    super(context, attrs);
    init(attrs, 0);
  }

  public FileTextView(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
    init(attrs, defStyle);
  }

  private void init(AttributeSet attrs, int defStyle)
  {
    // Load attributes
    final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.FileTextView, defStyle, 0);
    this.fileName = a.getString(R.styleable.FileTextView_file_name);
    a.recycle();
    StringBuilder sb = new StringBuilder();
    InputStream is = null;
    BufferedReader br = null;

    try
    {
      is = this.getContext().getAssets().open(fileName);
      br = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
      String line;
      String lf = System.getProperty("line.separator");
      while ((line = br.readLine()) != null)
      {
        sb.append(line).append(lf);
      }
    }
    catch (IOException e)
    {
      Log.e(TAG, "[init] : e" + e.getMessage(), e);
    }
    finally
    {
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch (IOException e)
        {
          Log.e(TAG, "[init] : " + e.getMessage(), e);
        }
      }

      if (br != null)
      {
        try
        {
          br.close();
        }
        catch (IOException e)
        {
          Log.e(TAG, "[init] : " + e.getMessage(), e);
        }
      }
    }

    this.setText(sb);
  }
}